Honey Hunters Test
============================

Установка:

1. Выполнить `composer global require "fxp/composer-asset-plugin:^1.3.1"`
1. Запустить `composer install`
2. Создать базу hhmt
3. Отредактировать файл 'config/db.php'
4. Выполнить миграцию `./yii migrate`