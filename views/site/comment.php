<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * Created by PhpStorm.
 * User: ivphpan
 * Date: 18.09.17
 * Time: 10:28
 * @var $this \yii\web\View
 * @var $model \app\models\Comment
 * @var $item \app\models\Comment
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

$this->title = 'Комменатарии';

$this->registerJs('
        $("#comment-form").on("pjax:end", function() {
            $.pjax.reload({container:"#comment-items"}); 
        });
');
?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-4 header--logo">
                <a href="/" class="header-link">
                    <img src="/images/header--logo.png" alt="" class="img-responsive">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-4 col-xs-5 col-xs-offset-3">
                <img src="/images/mail-icon.png" alt="" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <?php  Pjax::begin(['id'=>'comment-form'])?>
            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'col-md-12 col-xs-12 comment-form']
            ]) ?>
            <div class="row">
                <div class="col-md-5">
                    <?= $form->field($model, 'name',[
                            'template'=>'
                            <label class="comment-form--label">Имя <span>*</span></label>
                            {input}{error}
                            '
                    ])->textInput(['class'=>'comment-form--input form-control']) ?>
                    <?= $form->field($model, 'email',[
                        'template'=>'
                            <label class="comment-form--label">Email <span>*</span></label>
                            {input}{error}
                            '
                    ])->textInput(['class'=>'comment-form--input form-control']) ?>
                </div>
                <div class="col-md-6 col-md-offset-1">
                    <?= $form->field($model, 'comment',[
                        'template'=>'
                            <label class="comment-form--label">Комментарий <span>*</span></label>
                            {input}{error}
                            '
                    ])->textarea(['class' => 'comment-form--comment form-control','rows'=>6]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-10 col-xs-12">
                    <?=Html::submitButton('Записать',['class'=>'btn comment-form--btn'])?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
            <?php  Pjax::end()?>
        </div>
    </div>
</header>
<article>
<div class="container">
    <h2 class="comment-h2">Выводим комменатрии</h2>
    <div class="row">
        <?php Pjax::begin(['id'=>'comment-items'])?>
        <?php foreach($dataProvider->models as $i=>$item):?>
        <div class="col-md-4 col-lg-3">
            <div class="panel panel-primary comment-item comment-item__<?=(($i+1)%2==0?'odd':'even')?>">
                <div class="panel-heading">
                    <div class="panel-title"><?=Html::encode($item->name)?></div>
                </div>
                <div class="panel-body">
                    <p class="comment-item--email"><?=Html::encode($item->email)?></p>
                    <p class="comment-item--comment"><?=Html::encode($item->comment)?></p>
                </div>
            </div>
        </div>
        <?php endforeach;?>
        <?php Pjax::end()?>
    </div>
</div>
</article>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <a href="">
                    <img src="/images/footer--logo.png" alt="" class="img-responsive footer--img">
                </a>
            </div>
            <div class="col-md-2 col-md-offset-7">
                <ul class="footer-social">
                    <li class="footer-social--item">
                        <a href="" class="footer--link">
                            <img src="/images/footer--vk.png" alt="" class="img-responsive footer--img">
                        </a>
                    </li>
                    <li class="footer-social--item">
                        <a href="" class="footer--link">
                            <img src="/images/footer--fb.png" alt="" class="img-responsive footer--img">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

