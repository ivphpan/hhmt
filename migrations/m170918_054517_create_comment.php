<?php

use yii\db\Migration;

class m170918_054517_create_comment extends Migration
{
    public $table = '{{%comment}}';
    public function safeUp()
    {
        $this->createTable($this->table,[
            'id'=>$this->primaryKey(),
            'name'=>$this->string()->notNull()->comment('Имя'),
            'email'=>$this->string()->notNull()->comment('Email'),
            'comment'=>$this->text()->notNull()->comment('Комментарий'),
            'created_at'=>$this->dateTime(),
            'updated_at'=>$this->dateTime(),
        ]);

        $this->execute("
INSERT INTO `comment` (`id`, `name`, `email`, `comment`, `created_at`, `updated_at`) VALUES
(3,	'Иван',	'ivan@yandex.ru',	'Привет как дела? Ну пожалуйста опубликуй!',	'2017-09-18 07:49:39',	'2017-09-18 07:49:39'),
(4,	'Мария',	'mariya@mail.ru',	'Привет, как дела?',	'2017-09-18 07:50:52',	'2017-09-18 07:50:52'),
(5,	'Дарья',	'darya@mail.ru',	'Привет, как дела?',	'2017-09-18 07:52:15',	'2017-09-18 07:52:15'),
(6,	'ivan',	'ivan@mail.ru',	'Привет, как дела?',	'2017-09-18 07:52:54',	'2017-09-18 07:52:54'),
(7,	'Sergey',	'sergey@mail.ru',	'Алло) Как дела? Ну',	'2017-09-18 07:54:54',	'2017-09-18 07:54:54'),
(8,	'Андрей',	'andrey@mail.ru',	'Привет! Меня зовут Андрей, мне 23 года!',	'2017-09-18 07:56:55',	'2017-09-18 07:56:55'),
(9,	'Привет',	'privet@mail.ru',	'Mail.ru самая лучшая почтовая служба!',	'2017-09-18 07:57:22',	'2017-09-18 07:57:22'),
(10,	'Афанасий',	'afan@mail.ru',	'\r\nНовые вакансии для резюме: Web-программист\r\nИван, мы нашли несколько вакансий, которые могут вам подойти.',	'2017-09-18 08:04:59',	'2017-09-18 08:04:59'),
(11,	'cvdsfdsfsdf',	'ivan@yandex.ru',	'dfsdfsdfsdfsdfdsfsdfsdfsdfsdfsd',	'2017-09-18 08:20:30',	'2017-09-18 08:20:30')");
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170918_054517_create_comment cannot be reverted.\n";

        return false;
    }
    */
}
