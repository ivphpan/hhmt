<?php
/**
 * Created by PhpStorm.
 * User: ivphpan
 * Date: 18.09.17
 * Time: 12:40
 */


namespace app\models;



use yii\data\ActiveDataProvider;

class CommentSearch extends Comment
{
    public function search(){
        $query = Comment::find();
        $dataProvider = new ActiveDataProvider([
            'query'=>$query,
            'sort'=>['defaultOrder'=>['created_at'=>SORT_DESC]],
        ]);
        return $dataProvider;
    }
}